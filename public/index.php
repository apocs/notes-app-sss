<?php
	//load all the class files
	require_once('autoload.php');

	//use the notes
	use App\Notes as Notes;

	//the notes constructor create a new notes controller
	//this needs an userID, objectID and objectType
	$notes = new Notes(/*user id*/1,/*object id*/1,/*object type*/1);

	if($_POST){
		//if post, save the data and reloads the page
		$notes->notes_saveForm();
		header("Location: ?mode=read");
	}

	if(isset($_GET['mode'])){
		$mode = $_GET['mode'];
	}else{
		$mode = 'read';
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Notes App</title>
		<link rel="stylesheet" href="css/notes.css">
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
	</head>
	<body>
		<div class="main-box">
			<form method="POST">
				<?php 
					// THIS LOADS THE NOTES FORM IN EDIT/READ MODE
					if( $mode == 'edit' || $mode == 'read' ){
						$notes->notes_outputForm($mode);
					}
					
				
					// THIS SHOW A SIMPLE TABLE WITH THE NOTES LIST
					if( $mode == 'simple' ){
						$notes->notes_outputRows_Read();
					}

						
					// THIS SHOW HOW THE NOTES ARRAY LOOKS
					if( $mode == 'array' ){
						echo '<pre>';
						print_r($notes->notes_getNotesArray());
						echo '</pre>';
					}
					 
					if($mode == 'edit'){
				?>
						<input type="submit" value="Save Notes" class="notes-submit button" />
						<a href="/" class="notes-submit button" style="right: 12%">Cancel</a>
				<?php
					}else{
				?>
						<a href="?mode=edit" class="notes-submit button">Edit Mode</a>
						<a href="?mode=simple" class="notes-submit button" style="right: 10.7%">Show Simple Table</a>
						<a href="?mode=array" class="notes-submit button" style="right: 24%">Show Array</a>
				<?php
						if($mode != 'read'){
				?>
						<a href="/" class="notes-submit button" style="right: 32.7%">Read Mode</a>
				<?php
						}
					}
				?>
			</form>
		</div>
	</body>
</html>