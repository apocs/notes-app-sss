//main javascript file

//globals
var newOnes = 0;
var newOnesIndex = [];
var editable = false;
var focusThis;
var currentContext;
var errorPresent = false;

//titles and text names
var MAX_TEXTAREA_LENGTH = 50;
var MIN_TEXTAREA_LENGTH = 3;
var MAX_LENGTH_MESSAGE = 'Notes max limit is 50 chars.';
var MIN_LENGTH_MESSAGE = 'Notes min limit is 3 chars.';


var MONTH_NAMES = { 
	"01":"Jan", 
	"02":"Feb", 
	"03":"Mar", 
	"04":"Apr", 
	"05":"May", 
	"06":"Jun", 
	"07":"Jul", 
	"08":"Agu", 
	"09":"Sep", 
	"10":"Oct", 
	"11":"Nov", 
	"12":"Dec"
};

var BTN_DELETE = 'Delete';
var BTN_DONE = 'Done';
var BTN_EDIT = 'Edit';
var BTN_CANCEL = 'Cancel';



//main document load
$(document).ready(function(){
    app();
});

function app(){
	currentContext = null;

	//hide and show delete button
	$(document).on({
	    mouseenter: onHoverShowNoteOptions,
	    mouseleave: onHoverHideNoteOptions
	}, '.note-element');

	//if the not editable notes element is click, change to edit mode of the note.
	$(document).on('click','.note-edit',activateEditionModeOnClickedNotesElements);
	$(document).on('click','.note-raw-text',activateEditionModeOnClickedNotesElements);
	
	//deactivate the editable mode of the note if the "ok" button is clicked.
	$(document).on('click','.note-update', deactivateEditionModeAndSave);

	//remove note element on click delete button
	$(document).on('click','.note-delete', removeNoteElementAndMarkNoteAsDeleteBeforeSave);

	//cancel edition, on new elements remove note, on updates reverts.
	$(document).on('click','.note-cancel', cancelEditionOfCurrentNote);

	//deactivate edition and save on focus out
	$(document).on('focusout','.note-text textarea', deactivateEditionModeAndSaveOnFocusOut);
	
	//deactivate edition on hit enter
	$(document).on('keypress','.note-text textarea', deactivateEditionModeAndSaveOnHitEnter);

	//when add a new note button clicked, create a new note element on the form
	$('.note-add-new').on('click', createANewNoteListElementForInsertOnSave);
	
	//make list sortable
	$( "#notes-list" ).sortable({
		cancel: 'li.fixed',
		stop: callbackWhenStopSortingOneNote
	});
	
	$("#notes-list").find('textarea').each(function(i,e) {
		$(this).bind('click.sortable mousedown.sortable',function(e){
	    	e.stopImmediatePropagation();
		});
	})
}

// HELPER METHODS 
function updateNoteTextFieldIfChange(textShow, textInput, noteElement){
	if(textShow.html() != textInput.val()){
		var text = textInput.val();
		//trim line breaks
		text = text.replace(/(\r\n|\n|\r)/gm," ");

		//trim multiple spaces
		text = text.replace(/\s\s+/g, " ");

		//escape html
		text = htmlspecialchars(text);

		//set the trim value on both, editable and non editable elements
		textShow.html(text);
		textInput.val(text);

		if(noteElement.find('.change') != null && noteElement.attr('id') != null){
			noteElement.find('.change').val(noteElement.attr('id'));
			// console.log(noteElement.attr('id'));
		}
	}
}

function revertNoteTextFieldIfChange(textShow, textInput, noteElement){
	if(textShow.html() != textInput.val()){
		var text = textShow.html();
		textShow.html(text);
		textInput.val(text);
	}
}

function activateEditionModeOnClickedNotesElements(e){
	e.preventDefault();
	if(!editable){
		// console.log('activate edition');
		var noteElement = $(this).parent().parent();
		var textInput = noteElement.find('textarea');
		var textShow = noteElement.find('.note-raw-text');
		
		//focus on this input
		focusThis = textInput;
		currentContext = this;
		editable = true;

		// if the textarea is hidden show it
		// and hide the not editable span text
		// then focus on textarea element
		if(textInput.hasClass('hide')){
			textShow.addClass('hide');
			textInput.removeClass('hide');
			textInput.focus();

			// if the ok button is hidden, show it and hide delete button always.
			showEditionModeOptions(noteElement);
			hideViewModeOptions(noteElement);
		}
	}else{
		deactivateEditionModeAndSave(e);
		if(!editable){
			activateEditionModeOnClickedNotesElements(e);
		}
	}
}

function deactivateEditionModeAndSave(e){
	e.preventDefault();
	var noteElement;
	if($(this).parent().parent().hasClass('note-element')){
		noteElement = $(this).parent().parent();
	}else{
		noteElement = $(currentContext).parent().parent();
	}
	
	if(noteElement.hasClass('fixed')){
		noteElement.removeClass('fixed');
	}

	var textShow = noteElement.find('.note-raw-text');
	var textInput = noteElement.find('textarea');
	var noteOptions = noteElement.find('.note-options');
	var noteError = noteElement.find('.note-error');		

	if (!$.trim(textInput.val())) {
		//if is blank, must check that if is a new note or and old
		if($(this).parent().parent().hasClass('note-element')){
			currentContext = this; //this is for know in wich context is executing the function
		}

		focusThis = null;
		editable = false;

		removeNoteElementAndMarkNoteAsDeleteBeforeSave(e);
	}else{
		if(textInput.val().length >= MIN_TEXTAREA_LENGTH && textInput.val().length <= MAX_TEXTAREA_LENGTH){

			if(!noteError.hasClass('hide')){
				noteError.addClass('hide');
				errorPresent = false;
			}

			// if show text is hidden
			// hide the textarea
			// check if the note text have changed for update the span non editable text
			// show the span text note element
			if(textShow.hasClass('hide')){
				textInput.addClass('hide');
				updateNoteTextFieldIfChange(textShow, textInput, noteElement);
				textShow.removeClass('hide');	

				//if delete button is hidden, show and hidde ok button
				hideEditionModeOptions(noteElement);
				//console.log('this is the id over: '+noteElement.attr('id'));
				if (noteElement.is(':hover')) {
					//console.log('is over, yeah');
				    showViewModeOptions(noteElement);
				}	
			}
			
			// unset focus, and make availabe to edit other field again.
			focusThis = null;
			editable = false;
		}else{
			// console.log('and issue happens');
			// console.log('length ' +textInput.val().length);
			// show error messages to the user
			if(textInput.val().length < MIN_TEXTAREA_LENGTH){
				noteError.text(MIN_LENGTH_MESSAGE);
			}else if(textInput.val().length > MAX_TEXTAREA_LENGTH){
				noteError.text(MAX_LENGTH_MESSAGE);
				// console.log('hit this if over 50');
			}

			if(noteError.hasClass('hide')){
				noteError.removeClass('hide');
			}

			if(focusThis!=null){
				focusThis.focus();
			}
			errorPresent = true;
		}
	}
}

function deactivateEditionModeAndSaveOnHitEnter(e){
	var code = (e.keyCode ? e.keyCode : e.which);
	// console.log(code);
	if( code == 13 ) {
		currentContext = this;
		deactivateEditionModeAndSave(e);
  	}
}

function deactivateEditionModeAndSaveOnFocusOut(e){
	var noteElement = $(this).parent().parent();
	currentContext = this;
	//this prevents that on focus out be executed when hit any of the options buttons
	if(!noteElement.find('.note-update').is(':hover') && !noteElement.find('.note-cancel').is(':hover')) {
		deactivateEditionModeAndSave(e);
		console.log('hitting this');
	}
}

function onHoverShowNoteOptions(){
	showViewModeOptions($(this));
	// console.log('on hover in');
}

function onHoverHideNoteOptions(){
	hideViewModeOptions($(this));
}

function removeNoteElementAndMarkNoteAsDeleteBeforeSave(e){
	e.preventDefault();
	
	var deleteElementID = null;
	
	if(!errorPresent && !editable){
		// current note element
		var noteElement;
		if($(this).parent().parent().hasClass('note-element')){
			noteElement = $(this).parent().parent();
		}else{
			noteElement = $(currentContext).parent().parent();
		}

		// check if the current note element id is for a new note element
		// or if is from an already created note element on the database
		if(newOnesIndex.indexOf(noteElement.attr('id')) > -1){

			// remove from the index of new elements
			delete newOnesIndex[newOnesIndex.indexOf(noteElement.attr('id'))];
			// remove from the DOM page
			noteElement.remove();

		}else{
			// if is not a new element, mark is as delete and hide the note element
			if(noteElement.find('.delete') != null && noteElement.attr('id') != null){
				noteElement.find('.delete').val(noteElement.attr('id'));
				noteElement.addClass('hide');
				deleteElementID = noteElement.attr('id');
			}
		}
		
		// must reset all the postions of the notes
		var posCount = 1;
		$('.note-element').each(function(i,e){
	
			if( deleteElementID != null && deleteElementID != $(this).attr('id') ){
				if(!$(this).hasClass('hide')){
					$(this).attr('data-order-pos', posCount);
					
					if($(this).find('.sort') != null){
						$(this).find('.sort').val(posCount);
					}
				
					if($(this).find('.change') != null && $(this).attr('id') != null){
						$(this).find('.change').val($(this).attr('id'));
					}
					
					posCount++;
				}
			}

	      });

		//unset editable
		editable = false;
		focusThis = null;
	}
}

function cancelEditionOfCurrentNote(e){
	e.preventDefault();

	// current note element
	var noteElement = $(this).parent().parent();
	var textShow = noteElement.find('.note-raw-text');
	var textInput = noteElement.find('textarea');
	var noteError = noteElement.find('.note-error');

	// check if the current note element id is for a new note element
	// or if is from an already created note element on the database
	if(!errorPresent){
		if(newOnesIndex.indexOf(noteElement.attr('id')) > -1){
			if(!$.trim(textInput.val()) && textInput.val() == textShow.html()) {
				// remove from the index of new elements
				delete newOnesIndex[newOnesIndex.indexOf(noteElement.attr('id'))];
				// remove from the DOM page
				noteElement.remove();
			}else if(!$.trim(textInput.val()) && textInput.val() != textShow.html()) {
				if(textShow.hasClass('hide')){
					textInput.addClass('hide');
					//revert edition of the text note
					revertNoteTextFieldIfChange(textShow, textInput, noteElement);
					textShow.removeClass('hide');	

					//if delete button is hidden, show and hidde ok button
					hideEditionModeOptions(noteElement);
					if (noteElement.is(':hover')) {
						//console.log('is over, yeah');
					    showViewModeOptions(noteElement);
					}	
				}
			}else{
				if(textShow.hasClass('hide')){
					textInput.addClass('hide');
					//revert edition of the text note
					updateNoteTextFieldIfChange(textShow, textInput, noteElement);
					textShow.removeClass('hide');	

					//if delete button is hidden, show and hidde ok button
					hideEditionModeOptions(noteElement);
					if (noteElement.is(':hover')) {
						//console.log('is over, yeah');
					    showViewModeOptions(noteElement);
					}	
				}
			}
		}else{
			if(textShow.hasClass('hide')){
				textInput.addClass('hide');
				//revert edition of the text note
				revertNoteTextFieldIfChange(textShow,textInput,noteElement);
				textShow.removeClass('hide');	

				//if delete button is hidden, show and hidde ok button
				hideEditionModeOptions(noteElement);
				if (noteElement.is(':hover')) {
					//console.log('is over, yeah');
				    showViewModeOptions(noteElement);
				}			
			}
		}
		
	}else{
		if(!$.trim(textShow.html())) {
				// remove from the index of new elements
				delete newOnesIndex[newOnesIndex.indexOf(noteElement.attr('id'))];
				// remove from the DOM page
				noteElement.remove();
		}else if(textInput.val() != textShow.html()) {
			if(textShow.hasClass('hide')){
				textInput.addClass('hide');
				//revert edition of the text note
				revertNoteTextFieldIfChange(textShow, textInput, noteElement);
				textShow.removeClass('hide');	

				//if delete button is hidden, show and hidde ok button
				hideEditionModeOptions(noteElement);
				if (noteElement.is(':hover')) {
					//console.log('is over, yeah');
				    showViewModeOptions(noteElement);
				}	
			}
		}
	}	

	if(!noteError.hasClass('hide')){
		noteError.addClass('hide');
	}
	//unset control variables
	editable = false;
	focusThis = null;
	errorPresent = false;
}

function createANewNoteListElementForInsertOnSave(e){
	e.preventDefault();
	if(!editable){
		var currentDate = chageDateDisplayFormat(new Date().toISOString().slice(0, 19).replace('T', ' ')); //the date is on mysql format
		// count the new ones notes elements
		// this is used for crate an temp id.
		newOnes++;
		newNoteTmpId = 'new-'+newOnes;
		//add the temp id to the index of new notes elements
		newOnesIndex.push(newNoteTmpId);

		// get the notes list elements
		var noteList = $('#notes-list');
		var noteListLastChild = $('#notes-list li:last-child');
		var lastChildPosition = parseInt(noteListLastChild.attr('data-order-pos'));
		var newNotePosition = lastChildPosition + 1;
		
		console.log('@@@ last child li position');
		console.log(lastChildPosition);

		//create note element, and the sub divisions
		var newNoteElement = $('<li>').addClass('note-element').addClass('fixed').attr('id', newNoteTmpId).attr('data-order-pos', newNotePosition);
		var newNoteDate = $('<div>').addClass('note-date').text(currentDate); 
		var newNoteText = $('<div>').addClass('note-text');
		var newNoteOpt = $('<div>').addClass('note-options');

		//create the options elements for the note
		var newNoteOptRemove = $('<a>');
		newNoteOptRemove.addClass('note-delete').addClass('hide').attr('data-id', newNoteTmpId).css({'margin-left':'3px'}).attr('href','#').text(BTN_DELETE);
		var newNoteOptOk = $('<a>');
		newNoteOptOk.addClass('note-update').attr('data-id', newNoteTmpId).attr('href','#').text(BTN_DONE);
		var newNoteOptEdit = $('<a>');
		newNoteOptEdit.addClass('note-edit').addClass('hide').attr('data-id', newNoteTmpId).attr('href','#').text(BTN_EDIT);
		var newNoteOptCancel = $('<a>');
		newNoteOptCancel.addClass('note-cancel').attr('data-id', newNoteTmpId).css({'margin-left':'3px'}).attr('href','#').text(BTN_CANCEL);

		//error span
		var newNoteError = $('<span>');
		newNoteError.addClass('note-error').addClass('hide');

		//create the note text boxes for show/edit
		var newNoteTextInput = $('<textarea>').addClass('note-options').attr('name', 'notes['+newNoteTmpId+'][noteText]');
		var newNoteHiddenPosValue = $('<input>').addClass('sort').attr('name', 'notes['+newNoteTmpId+'][sort]').attr('type', 'hidden').attr('value', newNotePosition);
		var newNoteTextShow = $('<div>').addClass('note-raw-text').addClass('hide');

		//place the options on the notes options section 
		newNoteOpt.append(
			newNoteOptEdit,
			newNoteOptRemove,
			newNoteOptOk,
			newNoteOptCancel,
			newNoteError
		);

		//place the note text area for show/edit on the note text area
		newNoteText.append(
			newNoteTextInput,
			newNoteTextShow
		);

		//place all the elements on the note element
		newNoteElement.append(
			newNoteHiddenPosValue,
			newNoteDate,
			newNoteText,
			newNoteOpt
		);

		//append the note element to the note list
		noteList.append(
			newNoteElement
		);
		

		focusThis = newNoteTextInput;
		editable = true;
		focusThis.focus();

	}else{
		focusThis.focus();
	}
}

function chageDateDisplayFormat(stringDate){
	var dateTime = stringDate.split(' ');
	var date = dateTime[0].split('-');
	// 1 Y, 2 M, 3 D
	var newDateFormatted = MONTH_NAMES[date[1]]+'. '+date[2]+', '+date[0];

	return newDateFormatted;
}

function htmlspecialchars(string){ 
	var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };

  // string = string.replace(/[&<>"']/g, function(m) { return map[m]; });
  return $('<span>').text(string).html();
 }

function showEditionModeOptions(noteElement){
	if(noteElement.find('.note-update').hasClass('hide')){
		noteElement.find('.note-update').removeClass('hide');
	}

	if(noteElement.find('.note-cancel').hasClass('hide')){
		noteElement.find('.note-cancel').removeClass('hide');
	}
}

function hideEditionModeOptions(noteElement){
	if(!noteElement.find('.note-update').hasClass('hide')){
		noteElement.find('.note-update').addClass('hide');
	}

	if(!noteElement.find('.note-cancel').hasClass('hide')){
		noteElement.find('.note-cancel').addClass('hide');
	}
}

function showViewModeOptions(noteElement){
	if(noteElement.find('.note-update').hasClass('hide') && noteElement.find('.note-cancel').hasClass('hide')){
		if(noteElement.find('.note-edit').hasClass('hide')){
			noteElement.find('.note-edit').removeClass('hide');
		}

		if(noteElement.find('.note-delete').hasClass('hide')){
			noteElement.find('.note-delete').removeClass('hide');
		}
	}
}

function hideViewModeOptions(noteElement){
	if(!noteElement.find('.note-edit').hasClass('hide')){
		noteElement.find('.note-edit').addClass('hide');
	}

	if(!noteElement.find('.note-delete').hasClass('hide')){
		noteElement.find('.note-delete').addClass('hide');
	}
}

function callbackWhenStopSortingOneNote(e,u){
	console.log($(u.item).prev().hasClass('fixed'));
	if($(u.item).prev().hasClass('fixed')){
		console.log('@@ enter to revert');
		var fixePos = parseInt($(u.item).prev().attr('data-order-pos'));
		var fixed = $(".fixed");
		var ind = $("#notes-list li").index(fixed);
		if(ind !== fixePos) {
			if(ind > fixePos) {
				fixed.prev().insertAfter(fixed);
			} else {
				fixed.next().insertBefore(fixed);
			}
		}
	}else{
		console.log('@@@ element dropped');
	    console.log($(u.item).attr('data-order-pos'));
	    console.log('@@@ element pushed');
	    console.log($(u.item).next().attr('data-order-pos'));
	    
	    var pushedElm = $(u.item).next();
	    var pullElm = $(u.item).prev();
	    var dragElm = $(u.item);
	    
	    var newPos = parseInt(pushedElm.attr('data-order-pos'));
	    var dragPos = parseInt(dragElm.attr('data-order-pos'));
	    
	    if(dragPos > newPos){
	    
	      dragElm.attr('data-order-pos', newPos)
	      var count = newPos + 1;
	      
	      $('.note-element').each(function(i,e){
	
	          if(parseInt($(this).attr('data-order-pos')) >= newPos){
	            if($(this).attr('id') != dragElm.attr('id')){
	               if(!$(this).hasClass('hide')){
		               $(this).attr('data-order-pos', count);
		              
						if($(this).find('.sort') != null){
							$(this).find('.sort').val(count);
						}
						
						if($(this).find('.change') != null && $(this).attr('id') != null){
							$(this).find('.change').val($(this).attr('id'));
						}
		              count++;
	               }
	             }
	          }
	      });
	      
	    }else{
	        console.log('@@@ new pos');
	    	  console.log(newPos);
	    	  if(isNaN(newPos)){
	        	console.log('@@@ is NaN');
	        	 newPos = parseInt(pullElm.attr('data-order-pos'));
	        }else{
	        	newPos = newPos-1;
	        }
	        dragElm.attr('data-order-pos', newPos)
	        var count = newPos - 1;
	
	        $($('.note-element').get().reverse()).each(function(i,e){
	
	            if(parseInt($(this).attr('data-order-pos')) <= newPos){
	            	
	              if($(this).attr('id') != dragElm.attr('id')){
	              	
		              	if(!$(this).hasClass('hide')){
			                $(this).attr('data-order-pos', count);
			                
			                if($(this).find('.sort') != null){
								$(this).find('.sort').val(count);
							}
							
							if($(this).find('.change') != null && $(this).attr('id') != null){
								$(this).find('.change').val($(this).attr('id'));
							}
				
			                count--;
		              	}
	               }
	            }
	        });
	    }
	    
	    if(dragElm.find('.change') != null && dragElm.attr('id') != null){
			dragElm.find('.change').val(dragElm.attr('id'));
			// console.log(dragElm.attr('id'));
		}
		
		if(dragElm.find('.sort') != null && dragElm.attr('data-order-pos') != null){
			dragElm.find('.sort').val(dragElm.attr('data-order-pos'));
			// console.log(dragElm.attr('id'));
		}
	}
}