<?php 
namespace App;

// Note model DML, this class is used for performs updates into database.
class Note {
	
	private $conn;
	private $date;

	function __construct(){
		$this->conn = new Connection();
		$this->date = date('Y-m-d H:i:s');
	}

	// insert new note record
	public function save($objectType, $objectID, $noteText, $userID, $sortPosition){
		if($noteText != null && $noteText != ''){
			$this->conn->prepare("	INSERT INTO tbl_notes (
												objectType, 
												objectID, 
												noteText, 
												notesStatus, 
												createdWhen,
												createdBy,
												lastUpdatedWhen,
												lastUpdatedBy,
												sort )
									VALUES (?,?,?,?,?,?,?,?,?)"
			);

			$this->conn->handler->bindValue(1, $objectType);
			$this->conn->handler->bindValue(2, $objectID);
			$this->conn->handler->bindValue(3, $noteText);
			$this->conn->handler->bindValue(4, 1);
			$this->conn->handler->bindValue(5, $this->date);
			$this->conn->handler->bindValue(6, $userID);
			$this->conn->handler->bindValue(7, $this->date);
			$this->conn->handler->bindValue(8, $userID);
			$this->conn->handler->bindValue(9, $sortPosition);

			$this->conn->handler->execute();
		}
	}

	// update a note record
	public function update($noteID, $objectType, $objectID, $noteText, $userID, $sortPosition){
		if($noteText != null && $noteText != ''){
			$this->conn->prepare("	UPDATE tbl_notes 
									SET objectType = ?,
										objectID = ?, 
										noteText = ?, 
										lastUpdatedWhen = ?,
										lastUpdatedBy = ?,
										sort = ?
									WHERE noteID = ?"
			);

			$this->conn->handler->bindValue(1, $objectType);
			$this->conn->handler->bindValue(2, $objectID);
			$this->conn->handler->bindValue(3, $noteText);
			$this->conn->handler->bindValue(4, $this->date);
			$this->conn->handler->bindValue(5, $userID);
			$this->conn->handler->bindValue(6, $sortPosition);
			$this->conn->handler->bindValue(7, $noteID);

			$this->conn->handler->execute();
		}
	}

	// mark as removed/deleted note record by setting the notesStatus to 0
	public function remove($noteID, $userID){
		$this->conn->prepare("	UPDATE tbl_notes 
								SET notesStatus = 0, 
									deletedWhen = ?,
									deletedBy = ?
								WHERE noteID = ?"
		);

		$this->conn->handler->bindValue(1, $this->date);
		$this->conn->handler->bindValue(2, $userID);
		$this->conn->handler->bindValue(3, $noteID);

		$this->conn->handler->execute();
	}
}