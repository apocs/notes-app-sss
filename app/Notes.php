<?php
namespace App;

// this class is the main controller of the app
class Notes {

	private $userID;
	private $objectType;
	private $objectID;
	private $conn;
	private $queryResults;

	// configuration for first build of the view
	private $BTN_DELETE = "Delete";
	private $BTN_DONE = "Done";
	private $BTN_EDIT = "Edit";
	private $BTN_CANCEL = "Cancel";
	private $BTN_NEW_NOTE = "Add New Note";

	private $MONTHS = [ 
			"01" => "Jan", 
			"02" => "Feb", 
			"03" => "Mar", 
			"04" => "Apr", 
			"05" => "May", 
			"06" => "Jun", 
			"07" => "Jul", 
			"08" => "Agu", 
			"09" => "Sep", 
			"10" => "Oct", 
			"11" => "Nov", 
			"12" => "Dec"
	];

	// params:
	// the current userID
	// the current working objectID
	// and the current objectType
	function __construct($userID, $objectID, $objectType) {

		//create the database conection
    	$this->conn = new Connection();
    	$this->userID = $userID;
    	$this->objectID = $objectID;
    	$this->objectType = $objectType;

    }

    //main method, builds the app
	public function notes_outputForm($formMode) { 
		
		//get notes from db
		$this->notes_getNotesFromDatabase();

    	//loads the front end
    	$this->notes_outputRows_Form($formMode);
	}

	//simple table with a list of notes
	public function notes_outputRows_Read(){
		//get notes from db
		$this->notes_getNotesFromDatabase();

		//loads a more simple way of view the notes
		require_once('./../resources/notes_simple_view.php');
	}

	//performs saves into db
	public function notes_saveForm() {
		//loop into all the notes
		// var_dump($_POST['notes']);
		foreach ($_POST['notes'] as $note) {
			//instanciate a new Note model DML
			$noteDML = new Note();

			//check for new ones
			if($note['id']!=null){
				//check for marked as deleted
				if($note['delete'] != '0' && $note['delete'] == $note['id']){
					//perform deletion of current note
					$noteDML->remove($note['id'], $this->userID);
				}else{
					//check if the note have been updated
					if($note['change'] != '0' && $note['change'] == $note['id']){
						//perform update of current note
						$noteDML->update($note['id'], $this->objectType, $this->objectID, filter_var($note['noteText'], FILTER_SANITIZE_STRING), $this->userID, $note['sort']);
					}
				}
			}else{
				//perform insert of current note
				$noteDML->save($this->objectType, $this->objectID, filter_var($note['noteText'], FILTER_SANITIZE_STRING), $this->userID, $note['sort']);
			}
		}
	}

	//HELPERS PUBLIC METHODS
	public function notes_getNotesArray(){
		//new notes array
		$notesArray = [];
		$count = 0;

		//get notes from db
		$this->notes_getNotesFromDatabase();

		//populate notes array
		foreach($this->queryResults as $row){
			$notesArray[$count][1] = $this->notes_chageDateDisplayFormat($row->lastUpdatedWhen);
			$notesArray[$count][2] = $row->noteText;
			$count++;
		}

		return $notesArray;
	}

	//HELPERS PRIVATE METHODS
	private function notes_outputRows_Form($formMode) {
		//for more cleaning code, separated the view into other file.
		require_once('./../resources/notes_form_view.php');
	} 

	private function notes_chageDateDisplayFormat($stringDate){
		$dateTime = explode(" ", $stringDate);
		$date = explode("-", $dateTime[0]);
		// 1 Y, 2 M, 3 D
		$newDateFormatted = $this->MONTHS[$date[1]].". ".$date[2].", ".$date[0];

		return $newDateFormatted;
	}

	private function notes_getNotesFromDatabase(){
		//prepare the query statement
		$this->conn->prepare("	SELECT * FROM tbl_notes
    							WHERE objectType = ?
    							AND objectID = ?
    							AND notesStatus = 1
    							ORDER BY sort ASC");

		//binding the values for the query
		$this->conn->handler->bindValue(1, $this->objectType);
		$this->conn->handler->bindValue(2, $this->objectID);

		//perform the query
    	$this->queryResults = $this->conn->execute();
	}
}
?>