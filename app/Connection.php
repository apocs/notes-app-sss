<?php
namespace App;

class Connection {
	
	private $conn;
	private $DB_host = "localhost";
	private $DB_name = "c9";
	private $DB_user = "apocsve";
	private $DB_pass = "";

	public $handler;

	function __construct() {
		try{
            $this->conn = new \PDO("mysql:host=" . $this->DB_host . ";dbname=" . $this->DB_name, $this->DB_user, $this->DB_pass);
        }catch(\PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
    }

    function prepare($statement){
    	$this->handler = $this->conn->prepare($statement);
    }

    function execute(){
    	$this->handler->execute();

    	return $this->handler->fetchAll(\PDO::FETCH_OBJ);
    }

}