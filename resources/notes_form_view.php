<div id="notes-container">
	<ul id="notes-list">
	<?php
		$count = 1;
		$last = 0;
		//loop into all the query notes
		foreach($this->queryResults as $row){
			$change = 0;
			// checks if have a position saved on db, if not, asigns one number.
			if($row->sort != 0){
				$position = $row->sort;
				$last = $row->sort;
			}else{
				if($last > 0){
					$count = $last;
					$count++;
				}
				
				$position = $count;
				$count++;
				$change = $row->noteID;
			}
	?>
			<li id="<?php echo $row->noteID; ?>" class="note-element" data-order-pos="<?php echo $position; ?>">
				<input type="hidden" name="notes[<?php echo $row->noteID; ?>][id]" class="id" value="<?php echo $row->noteID; ?>">
				<input type="hidden" name="notes[<?php echo $row->noteID; ?>][sort]" class="sort" value="<?php echo $position; ?>">
				<input type="hidden" name="notes[<?php echo $row->noteID; ?>][delete]" class="delete" value="0">
				<input type="hidden" name="notes[<?php echo $row->noteID; ?>][change]" class="change" value="<?php echo $change; ?>">
				<div class="note-date"><?php echo $this->notes_chageDateDisplayFormat($row->lastUpdatedWhen); ?></div>
				<div class="note-text">
					<textarea name="notes[<?php echo $row->noteID; ?>][noteText]" class="hide"><?php echo $row->noteText; ?></textarea>
					<div class="note-raw-text"><?php echo $row->noteText; ?></div>
				</div>
				<div class="note-options">
					<a href="#" data-id="<?php echo $row->noteID; ?>" class="note-edit hide"><?php echo $this->BTN_EDIT; ?></a>
					<a href="#" data-id="<?php echo $row->noteID; ?>" class="note-delete hide"><?php echo $this->BTN_DELETE; ?></a>
					<a href="#" data-id="<?php echo $row->noteID; ?>" class="note-update hide"><?php echo $this->BTN_DONE; ?></a>
					<a href="#" data-id="<?php echo $row->noteID; ?>" class="note-cancel hide"><?php echo $this->BTN_CANCEL; ?></a>
					<span class="note-error hide"></span>
				</div>
			</li>
	<?php 
    	} //end of the foreach
	?>
	</ul>
	
	<?php if($formMode == 'edit'){ ?> <a href="#" class="note-add-new"><?php echo $this->BTN_NEW_NOTE; ?></a> <?php	} ?>
	
</div>
<?php if($formMode == 'edit'){ ?> <script src="js/notes.js"></script> <?php	} ?>