<div id="notes-container">
	<table class="simple">
		<thead>
			<tr>
				<th>Position</th>
				<th>Date</th>
				<th>Text Note</th>
			</tr>
		</thead>
		<tbody>
			<?php
				//loop into all the query notes
				foreach($this->queryResults as $row){
			?>
				<tr>
					<td><?php echo $row->sort; ?></td>
					<td><?php echo $this->notes_chageDateDisplayFormat($row->lastUpdatedWhen); ?></td>
					<td><?php echo $row->noteText; ?></td>
				</tr>
			<?php 
		    	} //end of the foreach
			?>
		</tbody>
	</table>
</div>
